package com.example.task12

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class DataModel:ViewModel() {
    val login: MutableLiveData<String> by lazy{
        MutableLiveData<String>()
    }

    val password:MutableLiveData<String> by lazy{
        MutableLiveData<String>()

    }
}