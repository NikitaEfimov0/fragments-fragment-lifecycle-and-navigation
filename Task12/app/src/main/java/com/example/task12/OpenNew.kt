package com.example.task12

interface OpenNew {
    fun openHome()
    fun openRegistration()
    fun checkData():Boolean
    fun saveNewData()
    fun exit()
}