package com.example.task12

import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.constraintlayout.widget.Placeholder
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelStore
import androidx.navigation.Navigation
import com.example.task12.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity(), OpenNew {
    var currentItem:Int = R.id.home
    lateinit var binding:ActivityMainBinding
    private val dataModel: DataModel? = null
    lateinit var sharedPtr:SharedPreferences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        //openFragment(LogInFragment.newInstance(), R.id.forFragment)
        binding.bottomNavigationView.visibility = View.INVISIBLE;
        sharedPtr = getSharedPreferences("Data", MODE_PRIVATE)
        binding.bottomNavigationView.setOnNavigationItemSelectedListener {

            when(it.itemId){
                R.id.home -> {
                    if(currentItem!=it.itemId) {
                        Navigation.findNavController(binding.fragmentContainerView)
                            .navigate(R.id.toHomeFromWeb)
                        currentItem = R.id.home
                    }
                    else
                        Navigation.findNavController(binding.fragmentContainerView).navigate(R.id.selfHome)
                }
                R.id.web-> {
                    if (currentItem != it.itemId) {
                        Navigation.findNavController(binding.fragmentContainerView)
                            .navigate(R.id.toWeb)
                        currentItem = R.id.web
                    }
                    else
                        Navigation.findNavController(binding.fragmentContainerView).navigate(R.id.selfWeb)
                }
            }
            true
        }
    }

    fun openFragment(fragment: Fragment, idHolder:Int){
        supportFragmentManager.beginTransaction().replace(idHolder, fragment).commit()

    }

    override fun openHome() {
        binding.bottomNavigationView.visibility = View.VISIBLE
        openFragment(HomeFragment.newInstance(), R.id.forFragment)
    }

    override fun openRegistration() {
        openFragment(RegistrationFragment.newInstance(), R.id.forFragment)
    }

    override fun saveNewData(){
        sharedPtr.edit().putString(UserData.Login, UserData.Login).apply()
        sharedPtr.edit().putString(UserData.Password, UserData.Password).apply()
    }

    override fun checkData():Boolean{
        if(sharedPtr.getString(UserData.Login, null) == UserData.Login && sharedPtr.getString(UserData.Password, null) == UserData.Password) {
            binding.bottomNavigationView.visibility = View.VISIBLE
            return true

        }
        else
            return false
    }

    override fun exit(){
        finishAffinity()
    }
}